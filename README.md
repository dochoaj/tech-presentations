# **Tech-Presentations** #

This repository contains all the presentations realized in HTML and CSS, that talks about tech topics.

### Main Objective ###

Nothing but collaborate!

### How do I get set up? ###

* The only requirement is to have a **modern** browser. So you just need to checkout and you're ready to go!

### Contribution guidelines ###

The presentations must:

* Talk about tech topics, software architecture, software development or coding in general.
* Be written in HTML/CSS/JS.
* Be located in corresponding folder.

### Who do I talk to? ###

If you need some help or have any issues, please contact me on:

* I'm [dochoaj](https://twitter.com/dochoaj) on Twitter.
* Or mail me to dochoaj at gmail.com, but I'm not very good replying emails.